public with sharing class AuraCmpAccCon {
    @auraEnabled
    public static List<VisualForceRec__c> getVisualData(){
        List<VisualForceRec__c> visList= new List<VisualForceRec__c>();
        visList=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c];
        return visList;
    }
    @auraEnabled
    public static List<VisualForceRec__c> getFilter(String filter){
        List<VisualForceRec__c> visList= new List<VisualForceRec__c>();
        Date today= Date.today();
        if(filter=='all'){
            visList=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c];
        }

        if (filter=='prev'){
            visList=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE VisualDate__c<:today];
        }

        if (filter=='next'){
            visList=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE VisualDate__c>:today];
        }
        return visList;
    }

    @auraEnabled
    public static List<VisualForceRec__c> saveRecords(String visualRecs, String accRecs, String conRecs){
        List<VisualForceRec__c> vfs= (List<VisualForceRec__c>)JSON.deserialize(visualRecs, List<VisualForceRec__c>.class);
        List<Account> accs= (List<Account>)JSON.deserialize(accRecs, List<Account>.class);
        List<Contact> cons= (List<Contact>)JSON.deserialize(conRecs, List<Contact>.class);
        List<String> accsId= new List<String>();
        System.debug(accs);
        System.debug(cons);
        System.debug(vfs);
        List<String> vfsId= new List<String>();
        for(VisualForceRec__c visRec: vfs){
            vfsId.add(visRec.Id);
        }
List<VisualForceRec__c> visUpdate = [SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE Id IN: vfsId];
        for (Integer i=0; i<accs.size();i++){
            visUpdate[i].VisualName__c= vfs[i].VisualName__c;
            visUpdate[i].VisualDate__c= vfs[i].VisualDate__c;
            visUpdate[i].Account__r.Name= accs[i].Name;
            visUpdate[i].Contact__r.LastName= cons[i].LastName;
        }
    update visUpdate;
    update accs;
    update cons;
    List<VisualForceRec__c> retList=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c];
    return retList;
    }
}

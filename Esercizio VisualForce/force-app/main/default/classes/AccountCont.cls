public class AccountCont {
    //Proprietà lista tabella da pagina visualforce.
    public List<VisualForceRec__c> vfRec {get; set;}
    //Proprietà filtro tabella da pagina visualforce.
    public String dateFilter {get; set;}
    //Proprietà parametri action function.
    public String visualName {get; set;}
    public Date visualData {get; set;}
    public String accName {get; set;}
    public String conName {get; set;}
    public String visualId {get; set;}

    //Costruttore che mi riempie proprietà tabella con query.
    public AccountCont() {
        vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c];
    }

    //Funzione del filtro data.
    public PageReference filtraData(){
        try{
            Date today= Date.today();
            if(dateFilter=='prev'){
                vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE VisualDate__c<:today];
                System.debug(vfRec);
            }
            if(dateFilter=='next'){
                vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE VisualDate__c>:today];
                System.debug(vfRec);
            }

            if(dateFilter=='all'){
                vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c];
                System.debug(vfRec);
            }
        } catch(DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
        return null;
    }

    //Funzione che deriva dalla action function
   // che salva il record agendo sui parametri della actionfunction
   //Restituisce un pageReference per navigare in un altra pagina visual force.
    public PageReference saveRecord(){
        PageReference finalPage = Page.myFinalPage;
        finalPage.setRedirect(true);
        try{
            VisualForceRec__c vf=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE Id=:visualId];
            vf.VisualName__c=visualName;
            vf.VisualDate__c=visualData;
            update vf;
            Account acc=[SELECT Id, Name FROM Account WHERE Id=:vf.Account__r.Id];
            acc.Name=accName;
            update acc;
            Contact con=[SELECT Id, LastName FROM Contact WHERE Id=:vf.Contact__r.Id];
            con.LastName=conName;
            update con;
        } catch(DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
        return finalPage;
    }
}

public class AccContr2 {
    public List<VisualForceRec__c> vfRec {get; set;}
    public List<VisualForceRec__c> vfRecList {get; set;}
    public String nameButton {get; set;}
    public Boolean isFilter {get; set;}
    public Boolean all {get; set;}
    public Boolean prev {get; set;}
    public Boolean next {get; set;}
    public Boolean isEdit {get; set;}
    //Proprietà parametri action function.
    public String idRec {get; set;}
    public String parId {get; set;}
    public String parVisName {get; set;}
    public String parVisDate {get; set;}
    public String parAccName {get; set;}
    public String parConName {get; set;}


    public AccContr2() {
        vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c];
        isFilter=false;
        nameButton='Mostra Filtro';
        isEdit=false;
        vfRecList=new List<VisualForceRec__c>();
    }

    public PageReference filtraData(){
        try{
            Date today= Date.today();
            if(prev){
                System.debug(prev);
                vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE VisualDate__c<:today];
                
            }
            if(next){
                vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE VisualDate__c>:today];
            }

            if(all){
                vfRec=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c];
            }
            
        } catch(DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
        return null;
    }

    public void showFilter(){
        if (isFilter==false){
            isFilter=true;
            nameButton='Nascondi Filtro';
        }
        else{
            isFilter=false;
            nameButton='Mostra Filtro';
        }
    }

    public void popRec(){
        System.debug('<<<<<<Arriva a pop rec');
        VisualForceRec__c vf=[SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE Id=:idRec];
        vfRecList.add(vf);
        System.debug(vfRecList);
    }

    //Funzione che deriva dalla action function
   // che salva il record agendo sui parametri della actionfunction
   //Restituisce un pageReference per navigare in un altra pagina visual force.
   public PageReference saveRec(){
    try{
        List<String> lstId = parId.split(',');
        List<String> lstVisName = parVisName.split(',');
        List<String> lstVisDate = parVisDate.split(',');
        List<String> lstAccName = parAccName.split(',');
        List<String> lstConName = parConName.split(',');
        List<VisualForceRec__c> vfRecords= [SELECT Id, VisualName__c, VisualDate__c, Account__r.Name, Contact__r.LastName FROM VisualForceRec__c WHERE Id IN:lstId];
        List<VisualForceRec__c> vfRecordsAdd= new List<VisualForceRec__c>(); 
        Account acc= new Account();
        Contact con= new Contact();
        List<Account> accAdd= new List<Account>();
        List<Contact> conAdd= new List<Contact>();
        List<String> vfIdsAcc= new List<String>();
        List<String> vfIdsCon= new List<String>();
            for(Integer i=0; i<lstId.size();i++){
                vfRecords[i].VisualName__c=lstVisName[i];
                vfRecords[i].VisualDate__c=date.parse(lstVisDate[i]);
                System.debug('<<<<<<<<<<<data '+vfRecords[i].VisualDate__c);
                //LOGICA ID
                //Prendo id account 1 e aggiungo a lista
                vfIdsAcc.add(vfRecords[i].Account__r.Id);
                System.Debug('1<<<<<<Arriva qui: '+vfIdsAcc);
                //Arriva lista id Account
                //EVITA QUERY DENTRO CICLI
                acc=[SELECT Id, Name FROM Account WHERE Id=: vfIdsAcc[i]];
                System.Debug('2<<<<<<Arriva qui: '+acc);
                //Arriva un account da aggiornare
                acc.Name=lstAccName[i];
                //LOGICA CONTACT
                //Prendo id account 1 e aggiungo a lista
                vfIdsCon.add(vfRecords[i].Contact__r.Id);
                System.Debug('1<<<<<<Arriva qui: '+vfIdsCon);
                //Arriva lista id Contact
                con=[SELECT Id, LastName FROM Contact WHERE Id=: vfIdsCon[i]];
                System.Debug('2<<<<<<Arriva qui: '+con);
                //Arriva un Contact da aggiornare
                con.LastName=lstConName[i];
                //Aggiungi contact aggiornato a lista account
                conAdd.add(con);
                System.Debug('3<<<<<<Arriva qui: '+conAdd);
            //Aggiungi account aggiornato
                accAdd.add(acc);
                    vfRecordsAdd.add(vfRecords[i]);
                    System.Debug('4<<<<<<Arriva qui: '+accAdd);
            }
            update vfRecordsAdd;
            System.debug('<<<<<<Fino a qui funziona'+vfRecordsAdd);
            update accAdd;
            update conAdd;
            vfRec= vfRecordsAdd;
    } catch(DMLException e){
        ApexPages.addMessages(e);
    }
    PageReference successPage = Page.myFinalPage;
    successPage.setRedirect(true);
    return successPage;
}
}

({
    doInit: function (component, event, helper) {
        helper.createTable(component, event, helper);
    },

    updateSelectedText: function (component, event, helper) {
        helper.selectRow(component, event, helper);
    },

    handleFilter: function (component, event, helper) {
        helper.mostraFiltri(component, event, helper);
    },

    handleFilterChange: function (component, event, helper) {
        helper.handlerFilter(component, event, helper);
    },

    updateRec: function (component, event, helper) {
        helper.saveRecords(component, event, helper);
    },

    resetForm: function (component, event, helper) {
        helper.reset(component, event, helper);
    },

    showToast: function (title, message, type) {
        helper.mostraToast(title, message, type);
    }
})

({
    createTable: function (component, event, helper) {
        //Crea tabella
        component.set("v.myColumns", [
            { label: "Visual Name", fieldName: "VisualName__c", type: "text" },
            { label: "Visual Date", fieldName: "VisualDate__c", type: "date" },
            { label: "Account Name", fieldName: "AccountName", type: "text" },
            { label: "Contact Name", fieldName: "ContactName", type: "text" },
        ]);
        var action = component.get("c.getVisualData");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Account__r) {
                        row.AccountName = row.Account__r.Name;
                        row.ContactName = row.Contact__r.LastName;
                    }
                }
                console.log(rows);
                component.set("v.visualRecs", rows);
            }
        });
        $A.enqueueAction(action);
    },

    selectRow: function (component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedRecs", selectedRows);
        component.set("v.showForm", true);
    },

    mostraFiltri: function (component, event, helper) {
        if (component.get("v.hideFilter") == false && component.get("v.showFilter") == true) {
            component.set("v.showFilter", false);
            var source = event.getSource();
            source.set("v.label", "Mostra Filtri");
        }
        else {
            component.set("v.showFilter", true);
            var source = event.getSource();
            source.set("v.label", "Nascondi Filtri");
            component.set("v.hideFilter", false);
        }
    },

    handlerFilter: function (component, event, helper) {
        component.set("v.loaded", true);
        var filter = event.getParam('value');
        var action = component.get("c.getFilter");
        action.setParams({ "filter": filter });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Account__r) {
                        row.AccountName = row.Account__r.Name;
                        row.ContactName = row.Contact__r.LastName;
                    }
                }
                console.log(rows);
                component.set("v.visualRecs", rows);
                component.set("v.loaded", false);
            }
            if (state == 'ERROR') {
                var errMess = response.getError()[0];
                this.mostraToast("Error", errMess, "error");
                component.set("v.loaded", false);
            }
        });
        $A.enqueueAction(action);
    },

    saveRecords: function (component, event, helper) {
        component.set("v.loaded", true);
        var visualRecs = component.get("v.selectedRecs");
        var visualList = [];
        var accs = [];
        var cons = [];
        visualRecs.forEach(function (elem) {
            var visualRec = {};
            visualRec.Id = elem.Id;
            visualRec.VisualName__c = elem.VisualName__c;
            visualRec.VisualDate__c = elem.VisualDate__c;
            visualList.push(visualRec);
            elem.Account__r.Name = elem.AccountName;
            elem.Contact__r.LastName = elem.ContactName;
            accs.push(elem.Account__r);
            cons.push(elem.Contact__r);

        })
        console.log(JSON.stringify(visualList));
        console.log(JSON.stringify(accs));
        console.log(JSON.stringify(cons));

        var action = component.get("c.saveRecords");
        action.setParams({
            "visualRecs": JSON.stringify(visualList),
            "accRecs": JSON.stringify(accs),
            "conRecs": JSON.stringify(cons)
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Account__r) {
                        row.AccountName = row.Account__r.Name;
                        row.ContactName = row.Contact__r.LastName;
                    }
                }
                console.log(rows);
                component.set("v.visualRecs", rows);
                this.mostraToast("Success", "Records Updated", "success");
                component.set("v.loaded", false);
            }

            if (state == 'ERROR') {
                var errMess = response.getError()[0];
                this.mostraToast("Error", errMess, "error");
                component.set("v.loaded", false);
            }
        });
        $A.enqueueAction(action);
    },

    reset: function (component, event, helper) {
        var selectedArr = [];
        component.set("v.selectedRecs", selectedArr);
        component.set("v.showForm", false);
        component.set("v.selRows", []);
    },

    mostraToast: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})
